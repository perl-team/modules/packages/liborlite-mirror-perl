Source: liborlite-mirror-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libfile-homedir-perl <!nocheck>,
                     libfile-remove-perl <!nocheck>,
                     libfile-sharedir-perl,
                     liblwp-online-perl <!nocheck>,
                     liborlite-perl <!nocheck>,
                     libparams-util-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liborlite-mirror-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liborlite-mirror-perl.git
Homepage: https://metacpan.org/release/ORLite-Mirror
Rules-Requires-Root: no

Package: liborlite-mirror-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libfile-homedir-perl,
         libfile-remove-perl,
         libfile-sharedir-perl,
         liblwp-online-perl,
         liborlite-perl,
         libparams-util-perl,
         libwww-perl
Description: ORLite extension to use remote SQLite databases
 ORLite::Mirror provides a simple mechanism for opening a read-only SQLite
 database from anywhere on the web. In a method analogous to using ORLite to
 work with a local read-only SQLite database, you can use ORLite::Mirror to
 open a SQLite database at an arbitrary URI. If the remote file has a "gz" or
 "bz2" extension, then ORLite::Mirror will decompress the file before opening.
